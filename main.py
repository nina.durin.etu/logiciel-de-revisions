import os
import sys
import csv
import time
import random
import hashlib
import platform
from rich import print
from rich.console import Console
from rich.table import Table
from rich.rule import Rule
from rich.panel import Panel
from rich.text import Text
console = Console()


global cahier_erreurs
global fichier_parametres
global correct_password_hash
cahier_erreurs = "data/Cahier d'erreurs.csv"
fichier_parametres = 'data/parameters.csv'
correct_password_hash = "1a5743fe22134b2c1c69c4ead1dcd192"




def check_password():
    password_prompt = Text("Entrez le mot de passe: ", style="bold yellow")
    panel = Panel(password_prompt, border_style="blue", expand=True, padding=(1, 2))
    console.print(panel, justify="center")

    user_password = input(' > ')
    user_password_hash = hashlib.md5(user_password.encode('utf-8')).hexdigest()

    if user_password_hash != correct_password_hash:
        console.print("Mot de passe invalide.", style="bold red")
        sys.exit()

def clear_screen():
    if(os.name == 'posix'):
       os.system('clear')
    else:
       os.system('cls')

def pause():
    print('Appuyez sur une touche pour continuer...')
    if platform.system() == "Windows":
        os.system("pause >nul")
    else:
        os.system("read -n 1 -s")

def write_parameters_to_csv(filename, parameters):
    with open(filename, "w", newline='', encoding='utf-8') as csvfile:
        writer = csv.writer(csvfile, delimiter=';')
        for key, value in parameters.items():
            writer.writerow([key, value])

def read_parameters_from_csv(filename):
    if not os.path.exists(fichier_parametres):
        default_parameters = {
            "parameter_erase_screen_on_question": "true",
            "parameter_question_color": "green",
            "parameter_answer_color": "blue",
            "parameter_shuffle_at_start": "true"
        }
        write_parameters_to_csv(fichier_parametres, default_parameters)

    parameters = {}
    with open(filename, "r") as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        for row in reader:
            parameters[row[0]] = row[1]
    return parameters

def display_parameters(parameters):
    table = Table(title="Parameters")
    table.add_column("Parameter", style="cyan")
    table.add_column("Value", style="magenta")

    for key, value in parameters.items():
        table.add_row(key, value)

    console.print(table)

def modify_parameter(parameters):
    while True:
        if parameters['parameter_erase_screen_on_question'] == "true":
            clear_screen()
        else:
            print('\n\n')
        console.rule("[bold yellow]Paramètres :", style='yellow')
        console.print("Quel paramètre modifier ?", style="bold yellow")
        for index, key in enumerate(parameters.keys()):
            console.print(f"{index + 1}. {key} ([cyan]{parameters[key]}[/cyan])")

        try:
            choice = int(console.input("\nEntrez le numéro de votre choix: "))
            if choice < 1 or choice > len(parameters):
                console.print("Choix invalide. Veuillez choisir un numéro de la liste.", style="bold red")
                continue

            selected_key = list(parameters.keys())[choice - 1]

            new_value = console.input(f"Quelle valeur utiliser pour '{selected_key}' ? \n > ")

            if selected_key.startswith("parameter_") and selected_key.endswith("_color"):
                parameters[selected_key] = new_value
            # elif selected_key == "parameter_erase_screen_on_question":
            else:
                if new_value.lower() in ["true", "false"]:
                    parameters[selected_key] = new_value.lower()
                else:
                    console.print("Valeur invalide. Entrez 'true' ou 'false'.", style="bold red")
                    continue

            write_parameters_to_csv(fichier_parametres, parameters)
            break

        except ValueError:
            console.print("Erreur de saisie. Veuillez entrer un numéro valide.", style="bold red")

def lire_csv(file_name):
    with open(file_name, newline='', encoding='utf-8') as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        data = [row for row in reader]
    return data

def trouver_fichiers_csv():
    fichiers_csv = []
    for fichier in os.listdir('data'):
        if fichier.endswith('.csv'):
            if fichier == os.path.basename(fichier_parametres): continue # skip parameters file
            fichiers_csv.append(f'data/{fichier}')
    fichiers_csv.sort()
    return fichiers_csv

def choisir_fichiers(fichiers):
    if not fichiers:
        console.print("[red]Erreur : Aucun fichier de questions.[/red]")
        return None

    table = Table(show_header=False, box=None, expand=True)
    table.add_column(justify="left")
    table.add_column(justify="right")

    table.add_row('[cyan]0[/cyan]. Aléatoire', '')

    nb_lignes_total = 0
    for index, fichier in enumerate(fichiers, start=1):
        with open(fichier, newline='', encoding='utf-8') as csvfile:
            reader = csv.reader(csvfile, delimiter=';')
            nb_lignes = sum(1 for _ in reader)
            nb_lignes_total += nb_lignes
        nom_fichier = os.path.splitext(os.path.basename(fichier))[0]
        table.add_row(f"[cyan]{index}[/cyan]. {nom_fichier}", f"[grey58]({nb_lignes} propositions)[/grey58]")
    table.add_row('', f'[grey58]([/grey58][grey82]{nb_lignes_total}[/grey82] [grey58]au total)[/grey58]')

    console.print("[bold yellow]Liste des fichiers de questions :[/bold yellow]")
    console.print(table)

    while True:
        while True:
            try:
                console.print('Entrez le/les numéro du fichier de questions à réviser : [grey58](Ex : "1,3,8" ou "4-8")[/grey58]')
                choix = input(" > ")
                if choix == 'p':
                    clear_screen()
                    modify_parameter(parameters)
                    clear_screen()
                    continue
                choix = choix.replace(" ", "") # Supprimer les espaces
                choix_split = [i.split('-') for i in choix.split(',')]
                break
            except ValueError:
                console.print("[red]Saisie invalide. Veuillez entrer un numéro ou une plage de numéros.[/red]")
        
        fichiers_choisis = []
        for element in choix_split:
            if len(element) == 1:
                index = int(element[0])
                if 0 <= index <= len(fichiers):
                    if index == 0:
                        fichiers_choisis.append(random.choice(fichiers))
                    else:
                        fichiers_choisis.append(fichiers[index - 1])
                else:
                    console.print("[red]Le numéro saisi est hors de la plage de choix. Veuillez réessayer.[/red]")
                    continue
            elif len(element) == 2:
                debut, fin = map(int, element)
                if 1 <= debut <= len(fichiers) and 1 <= fin <= len(fichiers) and debut < fin:
                    fichiers_choisis.extend(fichiers[debut - 1:fin])
                else:
                    console.print("[red]La plage de choix est invalide. Veuillez réessayer.[/red]")
                    continue
            else:
                console.print("[red]Saisie invalide. Veuillez entrer un numéro ou une plage de numéros.[/red]")
                continue

        if fichiers_choisis:
            contenu_fichiers = []
            for fichier in fichiers_choisis:
                with open(fichier, newline='', encoding='utf-8') as csvfile:
                    reader = csv.reader(csvfile, delimiter=';')
                    contenu_fichiers.extend(list(reader))

            console.print(f"\n[green]Fichiers choisis : [/green]")
            for fichier in fichiers_choisis:
                nom_simple = os.path.splitext(os.path.basename(fichier))[0]
                console.print(f"[green] - {nom_simple}[/green]")
            print('\n')
            return contenu_fichiers

def supprimer_doublons(fichier):
    lignes = []
    lignes_uniques = []

    with open(fichier, newline='', encoding='utf-8') as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        for row in reader:
            if row[0] not in lignes_uniques:
                lignes_uniques.append(row[0])
                lignes.append(row)

    with open(fichier, 'w', newline='', encoding='utf-8') as csvfile:
        writer = csv.writer(csvfile, delimiter=';')
        writer.writerows(lignes)

def verifier_csv(fichiers_csv):
    erreurs = []

    for fichier in fichiers_csv:
        if fichier == fichier_parametres: continue # skip parameters file
        supprimer_doublons(fichier)
        with open(fichier, newline='', encoding='utf-8') as csvfile:
            reader = csv.reader(csvfile, delimiter=';')
            for index, row in enumerate(reader, start=1):
                if len(row) != 3:
                    erreurs.append((fichier, index, "nombre de colonnes incorrect"))
                elif row[1].lower() not in ["vrai", "faux"]:
                    erreurs.append((fichier, index, "la deuxième colonne doit contenir 'Vrai' ou 'Faux'"))

    if erreurs:
        console.print("[red]Attention, des erreurs ont été trouvées dans la base de données :[/red]")
        for fichier, ligne, erreur in erreurs:
            console.print(f" - [bold yellow]{fichier}[/bold yellow], ligne [bold cyan]{ligne}[/bold cyan] : {erreur}.")
        quit()
    else:
        console.print("[green]Base de données vérifiée avec succès.[/green]")

def ajouter_ligne_csv(fichier_csv, ligne):
    with open(fichier_csv, 'a', newline='', encoding='utf-8') as csvfile:
        writer = csv.writer(csvfile, delimiter=';')
        writer.writerow(ligne)

    with open(fichier_csv, 'r', newline='', encoding='utf-8') as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        nb_lignes = sum(1 for _ in reader)

    print(f"[bright_black]La proposition suivante a été ajoutée avec succès : \n\"{ligne[0]}\" ({ligne[1]}). \nLe fichier {fichier_csv} contient maintenant [grey52]{nb_lignes}[/grey52] lignes.[/bright_black]\n")

def supprimer_ligne(ligne_a_supprimer):

    lignes = []

    deleted_lines_number = 0
    with open(cahier_erreurs, newline='', encoding='utf-8') as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        for row in reader:
            if row[0] != ligne_a_supprimer[0]:
                lignes.append(row)
            else:
                deleted_lines_number += 1

    if deleted_lines_number == 0:
        console.print("[red]Erreur : Cette proposition n'est pas présente dans le cahier d'erreurs. \nIl est interdit de suprimmer une proposition en dehors du cahier d'erreurs.[/red]")
        return

    with open(cahier_erreurs, 'w', newline='', encoding='utf-8') as csvfile:
        writer = csv.writer(csvfile, delimiter=';')
        writer.writerows(lignes)

    with open(cahier_erreurs, 'r', encoding='utf-8') as file:
        line_number = len(file.readlines())

    print(f"[bright_black] La proposition suivante a été supprimée avec succès : \n {ligne_a_supprimer[0]} ({ligne_a_supprimer[1]}). \nLe fichier {cahier_erreurs} contient maintenant {line_number} lignes.[/bright_black]\n")
    print('')



def poser_questions(data):
    def next_question():
        if parameters['parameter_erase_screen_on_question'] == "true":
            pause()
            clear_screen()
        else:
            print('\n\n')

    encouragements = ["Bravo!", "Super!", "Excellent!", "Bien joué!", "Continuez comme ça!", 'Courage !', 'Allez-y !', 'Allez !', 'Bravo !', 'Top !', 'Yes !', 'Youpi !', 'Génial !', 'Super !', 'Formidable !', 'Excellent !', 'Fantastique !', 'Magnifique !', 'Splendide !', 'Admirable !', 'Exceptionnel !', 'Époustouflant !', 'Sensationnel !', 'Merveilleux !', 'Éblouissant !', 'Épatant !', 'Étonnant !', 'Incroyable !', 'Stupéfiant !', 'Phénoménal !', 'Sublime !', 'Élégant !', 'Remarquable !', 'Impeccable !', 'Parfait !', 'Irrésistible !', 'Charmant !', 'Ravissant !', 'Émouvant !', 'Touchant !', 'Enthousiasmant !', 'Motivant !', 'Encourageant !', 'Inspirant !', 'Exaltant !', 'Énergisant !', 'Revigorant !', 'Réconfortant !', 'Rassurant !', ]
    bonnes_reponses = ['Exactement !', 'C\'est ça !', 'Tout à fait !', 'Absolument !', 'Bien joué !', 'Bravo !', 'Excellent choix !', 'Excellent travail !', 'Très bien !', 'Parfaitement juste !', 'Superbe réponse !',]
    mauvaises_reponses = ["Malheureusement non.", "Ce n'est pas la bonne réponse.", "Dommage, ce n'est pas ça.", "Non, désolé.", "Incorrect.", "Mauvaise réponse.", "Ce n'est pas la réponse attendue.", "Essayez encore.", "Ce n'est pas ce que nous cherchons.", "Ce n'est pas juste.", "Vous avez manqué la bonne réponse.", "Ce n'est pas la bonne réponse.", "Cette réponse n'est pas correcte.", "Non, ce n'est pas ça.", "Pas tout à fait.", "Ce n'est pas le choix correct.", "Faux.", "Malheureusement, ce n'est pas correct.", "Cela ne répond pas à la question.", "La réponse n'est pas exacte.", "Vous n'êtes pas sur la bonne voie.", "Ce n'est pas la réponse que nous cherchions.", "Non, désolé, ce n'est pas bon.", "Vous devez essayer à nouveau.", "Malheureusement, vous vous êtes trompé.", "Ce n'est pas la bonne réponse, je suis désolé.", "Cette réponse ne correspond pas à la question.", "Votre réponse est incorrecte.", "Ce n'est pas le choix approprié.", "Non, ce n'est pas la réponse correcte.", ]
    nombre_reponses_avant_information_avancement = int(len(data) / 10) if int(len(data) / 10) != 0 else 10

    documentation = """
     - Vrai/v/1 : Si vous pensez que la proposition est vraie
     - Faux/f/2 : Si vous pensez que la proposition est fausse
     - w        : Voir l'avancement actuel
     - s        : Sauvegarder la proposition précédente dans un fichier séparé.
     - x        : Supprimmer la proposition précédente (uniquement pour le cahier d'erreurs)
     - p        : Changer les paramètres d'affichage
     - skip/pass: Passer des questions
     - q        : Quitter
    """

    if parameters['parameter_shuffle_at_start'] == "true":
        random.shuffle(data)

    if parameters['parameter_erase_screen_on_question'] == "true":
        clear_screen()
    
    avancement = 0
    nombre_reponses = 0
    nombre_reponses_correctes = 0
    ancient_row = ''
    to_skip = 0
    skipped = 0
    combo = 0
    meilleur_combo = 0
    for row in data:
        if skipped < to_skip:
            avancement += 1
            skipped += 1
            continue
        else:
            to_skip = 0

        question, reponse, explication = row

        if avancement % nombre_reponses_avant_information_avancement == 0:
            reste = len(data) - avancement
            print(f'[bright_black]Vous avez répondu à {avancement} questions. Il y en a encore {reste}.[/bright_black]')

        valable = False
        while valable != True:
            combo_str = f" ----- [COMBO : {combo}]" if combo >= 10 else ""
            console.rule(f"[bold yellow]Question {avancement}/{len(data)} :{combo_str}", style='yellow')
            console.print(f"[bold yellow]{question}[/bold yellow] [grey58](vrai/faux)[/grey58] \n", end=' ')
            user_answer = input(" >> ").lower()
            if user_answer in ['v', 'vrai', 'o', 'y', '2']:
                user_answer = 'Vrai'
            elif user_answer in ['f', 'faux', 'n', '1', '0']:
                user_answer = 'Faux'
            else:
                if user_answer == 'q':
                    ratio = nombre_reponses_correctes / nombre_reponses if nombre_reponses != 0 else 0
                    print(f'\n[cyan]Voici vos stats : \n - Nombre de réponses : {nombre_reponses}\n - Nombre de réponses correctes : {nombre_reponses_correctes}/{nombre_reponses} \n - Nombre de réponses incorrectes : {nombre_reponses-nombre_reponses_correctes}/{nombre_reponses} \n - Ratio de bonnes réponses : {round(ratio*100, 1)}% ({round(ratio*20, 2)}/20) \n - Meilleur combo : {meilleur_combo} [/cyan]\n[bold gold1]Fellicitations ![/bold gold1]\n')
                    print('[blue]Fermeture du programme.[/blue]')
                    exit()
                elif user_answer == 's':
                    if parameters['parameter_erase_screen_on_question'] == "true": 
                        clear_screen() 
                        print()
                    ajouter_ligne_csv(cahier_erreurs, ancient_row)
                elif user_answer == 'x':
                    if parameters['parameter_erase_screen_on_question'] == "true": 
                        clear_screen() 
                        print()
                    supprimer_ligne(ancient_row)
                elif user_answer == 'w':
                    if parameters['parameter_erase_screen_on_question'] == "true": 
                        clear_screen() 
                        print()
                    reste = len(data) - avancement
                    ratio = nombre_reponses_correctes / nombre_reponses if nombre_reponses != 0 else 0
                    print(f'[cyan]Voici où vous en êtes : \n - Nombre de réponses : {nombre_reponses}\n - Nombre de propositions restantes : {reste}\n - Nombre de réponses correctes : {nombre_reponses_correctes}/{nombre_reponses} \n - Nombre de réponses incorrectes : {nombre_reponses-nombre_reponses_correctes}/{nombre_reponses} \n - Ratio de bonnes réponses : {round(ratio*100, 1)}% ({round(ratio*20, 2)}/20) \n - Meilleur combo : {meilleur_combo}  \n - Combo actuel : {combo} [/cyan]\n[bold gold1]{random.choice(encouragements)}[/bold gold1]\n')
                elif user_answer == 'p':
                    clear_screen()
                    modify_parameter(parameters)
                elif user_answer == 'skip' or user_answer == 'pass':
                    clear_screen()
                    to_skip = input('Combien de questions souhaitez-vous passer ?\n > ')
                    try:
                        to_skip = int(to_skip) - 1
                        valable = True
                        skipped = 0
                        avancement += 1
                    except ValueError:
                        print('Veuillez donner un nombre valide.')
                        pass
                else:
                    console.print(f'[red]Réponse invalide. \nVeuillez répondre par : {documentation}[/red]')
                next_question()
                continue
            valable = True
        
        if to_skip > 0: continue

        print('Vous avez répondu : "{}"'.format(user_answer))

        if user_answer.lower() == reponse.lower():
            combo += 1
            if meilleur_combo < combo: meilleur_combo = combo
            encouragement = random.choice(encouragements)
            bonne_reponse = random.choice(bonnes_reponses)
            console.print(f"[green]{bonne_reponse} {encouragement}[/green]")
            nombre_reponses_correctes += 1
            if explication:
                console.print(f"[cyan]En effet, {explication}[/cyan]")
        else:
            combo = 0
            mauvaise_reponse = random.choice(mauvaises_reponses)
            console.print(f"[red]{mauvaise_reponse}! La bonne réponse est :[/red] [magenta]{reponse}[/magenta].")
            if explication:
                console.print(f"[cyan]Explication : {explication}[/cyan]")
            else:
                print('[cyan]Explication : Aucune.[/cyan]')
        console.print()
        next_question()
        avancement += 1
        nombre_reponses += 1
        ancient_row = tuple(row)
        continue

    ratio = nombre_reponses_correctes / nombre_reponses if nombre_reponses != 0 else 0
    print(f'\n[cyan]Voici vos stats : \n - Nombre de réponses : {nombre_reponses}\n - Nombre de réponses correctes : {nombre_reponses_correctes}/{nombre_reponses} \n - Nombre de réponses incorrectes : {nombre_reponses-nombre_reponses_correctes}/{nombre_reponses} \n - Ratio de bonnes réponses : {round(ratio*100, 1)}% ({round(ratio*20, 2)}/20) \n - Meilleur combo : {meilleur_combo} [/cyan]\n[bold yellow]Fellicitations ![/bold yellow]\n')
    print('[blue]Fermeture du programme.[/blue]')

    return None



def main():
    clear_screen()
    # check_password()
    global parameters
    parameters = read_parameters_from_csv(fichier_parametres)
    fichiers_csv = trouver_fichiers_csv()
    verifier_csv(fichiers_csv)
    data = choisir_fichiers(fichiers_csv)
    # fichier_csv = choisir_fichier(fichiers_csv)
    # data = lire_csv(fichier_csv)
    poser_questions(data)
    # time.sleep(10)

if __name__ == "__main__":
    main()
