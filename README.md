# Logiciel d'entraînement pour étudiant en médecine

![Capture d'écran du programme](./Capture_d_écran_1.png)  
![Capture d'écran du programme](./Capture_d_écran_2.png)  

---

## Introduction

Ce logiciel est un outil puissant pour tout étudiant en médecine. Sa base de données contient ~5500 questions-réponses et l'explication associée. Les cours concernés sont ceux trouvés en première année, mais il est très simple d'en ajouter de nouveaux. Il est basé sur des propositions vraies ou fausses, donc particulièrement adapté pour s'entraîner aux QCM.

## Fonctionnalités

 - Une grande quantité de questions portant sur beaucoup de cours différents
 - Un choix du cours souhaité, avec possibilité de combiner les cours pour augmenter la difficulté
 - Une fonctionnalité d'aléatoire permettant l'appel des questions dans un ordre changant à chaque fois
 - Un outil de vérification de la base de données, permettant d'identifier les lignes ayant une mauvaise syntaxe pour faciliter l'ajout
 - Une interface simple et facile à prendre en main
 - Une installation très simple et rapide
 - Un cahier d'erreurs permettant l'ajout des cartes les plus difficiles pour insister dessus, pendant l'apprentissage ou la veille d'un examen
 - Un fonctionnement bassé sur les mécanismes de mémoire, grâce à la répétition et l'espacement
 - Des paramètres de gestion de l'affichage : effacement (et couleurs, en cours)

## Cas d'utilisation

L'étudiant en médecine trouvera ce logiciel d'une aide incroyable, alliant rapidité et simplicité. Il lui permettra de réviser une quantité énorme d'informations, tout en s'entraînant aux QCM. Il aura alors une avance incroyable sur les autres, car le logiciel entraîne l'étudiant à répondre bien mais aussi à réponde vite, donc plus de problêmes pour répondre instinctivement à toutes les propositions d'un QCM.

## Prérequis

 - [Python](https://www.python.org/)
 - Module Pygame pour python (`pip install pygame`)
 - Module Rich pour python (`pip install rich`)

## Installation

`pip install pygame`  
`pip install rich`  
`git clone https://gitlab.univ-lille.fr/nina.durin.etu/logiciel-de-revisions.git`  

## Démarrage rapide

`cd logiciel-de-revisions`  
`python3 main.py`

## Licence

[`CC BY-NC-SA 4.0`](https://creativecommons.org/licenses/by-nc-sa/4.0/)  
Ce projet est sous licence creative commons **Attribution-NonCommercial-ShareAlike 4.0 International** (***CC BY-NC-SA 4.0***), il peut donc être librement partagé, modifié tant que le crédit approprié est rendu, mais jamais dans un but commercial et toujours avec la même license que l'original.
